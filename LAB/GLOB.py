

import pathlib
THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

from os.path import dirname, join, normpath
SEARCH = normpath (join (THIS_FOLDER, "../.."))

import glob
FINDS = glob.glob (SEARCH + '/**/*.py', recursive = True)

for FOUND in FINDS:
	print (FOUND)
