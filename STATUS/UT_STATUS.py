

'''
	https://docs.python.org/3/library/unittest.html#module-unittest
'''

'''
	python -m unittest UT_STATUS.py

	python -m unittest *STATUS.py
'''

STATUS = "LOCAL"
#STATUS = "PYPI"

MODULES_PATHS = []
if (STATUS == "LOCAL"):
	MODULES_PATHS = [ 'THIS_MODULE', 'PIP_THIS_MODULE_REQUIREMENTS' ]
	
if (STATUS == "PYPI"):
	MODULES_PATHS = [ 'PIP_THIS_MODULE', 'PIP_THIS_MODULE_REQUIREMENTS' ]


def ADD_PATHS_TO_SYSTEM (PATHS):
	import pathlib
	FIELD = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	import sys
	for PATH in PATHS:
		sys.path.insert (0, normpath (join (FIELD, PATH)))

ADD_PATHS_TO_SYSTEM (MODULES_PATHS)


import HEALTH_MONITOR

import time
import unittest
class CONSISTENCY (unittest.TestCase):
	def test_1 (THIS):
		print ("test_1")

		import pathlib
		THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

		from os.path import dirname, join, normpath
		SEARCH = normpath (join (THIS_FOLDER, "STASIS/1"))

		import HEALTH_MONITOR
		HEALTH_MONITOR.START (
			GLOB = SEARCH + '/**/*HEALTH.py'
		)

		THIS.assertEqual (True, True)


'''
if __name__ == '__main__':
	unittest.main ()
'''