
#import os
#print (os.environ ['HOME'])

print ("@ CHECK_STATUS.py")

def OPEN_FLASK (
	PORT = 0
):
	print ("OPENING FLASK ON PORT:", PORT)

	from flask import Flask

	app = Flask (__name__)

	@app.route ("/")
	def HOME ():
		return "<p>HOME</p>"
		
	app.run (
		port = PORT
	)
	
	

def CLICK_FLASK ():
	import click
	@click.group ("FLASK")
	def GROUP ():
		pass

	'''
		./STATUS_CHECK FLASK OPEN \
		--port 10000
	'''
	@GROUP.command ("OPEN")
	@click.option ('--port', required = True)	
	def OPEN (port):
		OPEN_FLASK (
			PORT = port
		)

		return;


	return GROUP
	
def START_CLICK ():
	import click
	@click.group ()
	def GROUP ():
		pass
		
	
	GROUP.add_command (CLICK_FLASK ())

	GROUP ()

START_CLICK ()



#
