
'''
	import pathlib
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	SEARCH = normpath (join (THIS_FOLDER, "../.."))

	DB_FOLDER = normpath (join (THIS_FOLDER, "DB_FOLDER"))

	import HEALTH_MONITOR
	HEALTH_MONITOR.START (
		GLOB 		= SEARCH + '/**/*STATUS.py',
		DB_FOLDER 	= DB_FOLDER
	)
'''

'''
# CHECK_1_HEALTH.py

def CHECK_1 ():
	print ("CHECK 1")
	
	# raise Exception ()
	
	return;
	

CHECKS = {
	"CHECK 1": CHECK_1
}
'''

'''
STATUS.py

#
#	FLASK SERVER
#

for CHECK in CHECKS:
	

'''


import glob

from BOTANIST.PORTS.FIND_AN_OPEN_PORT import FIND_AN_OPEN_PORT

from .START_MULTIPLE_PROCESSES import START_MULTIPLE_PROCESSES


def CHECK_STATUS_LOCATION ():
	import pathlib
	THIS_FOLDER = pathlib.Path (__file__).parent.resolve ()

	from os.path import dirname, join, normpath
	CHECK_STATUS = normpath (join (THIS_FOLDER, "STATUS_CHECK.py"))
	
	return CHECK_STATUS

def START (
	GLOB 		= "",
	#DB_FOLDER 	= DB_FOLDER
):
	FINDS = glob.glob (GLOB, recursive = True)
	
	START_PORT = 40000;
	PORT = START_PORT;
	
	PORTS = []
	PROCESSES = []
	for FOUND in FINDS:
		print (FOUND)
		
		PORTS.append (PORT)
		
	
		PROCESSES.append ({
			"STRING": f'python3 { CHECK_STATUS_LOCATION () }  FLASK OPEN --port { PORT }',
			"CWD": None
		})
		
		PORT += 1;
		
	print ("FINDS:", len (FINDS))

	PROCS = START_MULTIPLE_PROCESSES (
		PROCESSES = PROCESSES
	)
	
	import time
	time.sleep (0.5)
	
	import requests
	r = requests.get (f'http://127.0.0.1:{ PORTS [0] }')	
	print (r.status_code)
	
	EXIT 			= PROCS ["EXIT"]
	PROCESSES 		= PROCS ["PROCESSES"]

	print (PROCS)

	
	
	#EXIT ()